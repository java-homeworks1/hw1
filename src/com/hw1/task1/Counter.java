package com.hw1.task1;

public class Counter {
    private Integer count;

    public Integer getCount() {
        return count;
    }

    public Counter(int value) { count = value; }
    public int increment() {
        count ++;
        return count;
    }
}
