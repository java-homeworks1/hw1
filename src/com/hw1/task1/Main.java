package com.hw1.task1;

public class Main {
    public static void main(String[] args) {
        Counter counter = new Counter(0);
        Timer timer = new Timer(counter);
        timer.run();
    }
}
