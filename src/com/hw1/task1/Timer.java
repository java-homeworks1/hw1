package com.hw1.task1;

public class Timer implements Runnable {
    private Counter counter;
    private Thread[] timePrinterList;

    public Timer(Counter counter) {
        this.counter = counter;
        this.timePrinterList = new Thread[]{
                new Thread(new TimePrinter(1000, this.counter)),
                new Thread(new TimePrinter(5000, this.counter))
        };
    }

    @Override
    public void run() {
        System.out.println("start thread");
        this.timePrinterList[0].start();
        this.timePrinterList[1].start();
        while (true) {
            try {
                counter.increment();
                Thread.sleep(1000);

            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }
}
