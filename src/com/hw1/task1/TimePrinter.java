package com.hw1.task1;

public class TimePrinter implements Runnable {
    private Counter counter;
    private Integer printInterval;

    public TimePrinter(Integer printInterval, Counter counter) {
        this.printInterval = printInterval;
        this.counter = counter;
    }

    @Override
    public void run() {
        while (true) {
            try {
                System.out.println("Print from " + this.printInterval.toString() + "; " + this.counter.getCount());
                Thread.sleep(this.printInterval);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }

}
