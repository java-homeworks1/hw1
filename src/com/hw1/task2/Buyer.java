package com.hw1.task2;

public class Buyer implements Runnable {
    private Storage storage;

    public Buyer(Storage storage) {
        this.storage = storage;
    }

    @Override
    public void run() {
        while (true) {
            try {
                synchronized (this.storage) {
                    if (this.storage.getGoodsCount() > 0) {
                        this.storage.setGoodsCount(this.storage.getGoodsCount() - 1);
                    }
                }
                Thread.sleep(1000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }
}
