package com.hw1.task2;

public class Provider implements Runnable {
    private Storage storage;

    public Provider(Storage storage) {
        this.storage = storage;
    }

    @Override
    public void run() {
        while (true) {
            try {
                synchronized (this.storage) {
                    if (this.storage.getGoodsCount() < 5) {
                        this.storage.setGoodsCount(this.storage.getGoodsCount() + 5);
                    }
                }
                Thread.sleep(1000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }
}
