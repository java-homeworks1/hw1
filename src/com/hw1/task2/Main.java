package com.hw1.task2;

public class Main {
    public static void main(String[] args) {
        Storage storage = new Storage();
        Buyer buyer = new Buyer(storage);
        Provider provider = new Provider(storage);
        new Thread(buyer).start();
        new Thread(provider).start();
        while (true) {
            try {
                System.out.println("Goods in storage = " + storage.getGoodsCount().toString());
                Thread.sleep(1000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }
}
